const path = require('path');


exports.generateSourceMaps = ({ type }) => ({
    devtool: type,
})

exports.loadIcons = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.(svg|png|eps)$/,
                include,
                exclude,
                use: {
                    loader: 'file-loader',
                    options
                }
            }
        ]
    }
});

exports.loadCSS = ({ include, exclude } = {}) => ({
    module: {
        rules: [
            {
                test: /\.css$/,
                include,
                exclude,
                use: ['style-loader', 'css-loader']
            }
        ]
    }
});

exports.loadSCSS = ({ include, exclude } = {}) => ({
    module: {
        rules: [
            {
                test: /\.(sass|scss)$/,
                include,
                exclude,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: "sass-loader",
                        options: {
                            sassOptions: {
                                outputStyle: "expanded",
                                includePaths: [
                                    path.resolve(__dirname, "./node_modules/compass-mixins/lib")                                ]
                            }
                        }
                    }
                    //'sass-resources-loader'
                ]
            }
        ]
    }
});


exports.devServer = ({ contentBase, host, port } = {} ) => ({
    devServer: {
        contentBase: contentBase,
        stats: "errors-only",
        host,
        port,
        historyApiFallback: true,
        overlay: {
            errors: true,
            warnings: true,
        },
        proxy: {
            '/api': 'http://localhost:8083'
        }
    },
});

