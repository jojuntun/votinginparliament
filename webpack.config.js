const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const parts = require('./webpack.parts');
const glob = require('glob');

const PATHS = {
    app: path.join(__dirname, 'client'),
    build: path.join(__dirname, 'build')
};

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './client/index.html',
    filename: 'index.html',
    inject: 'body'
});



const commonConfig = merge(
    {
        module: {
            rules: [
                { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
                { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
            ]
        },
        plugins: [HtmlWebpackPluginConfig],
    }
);

const developmentConfig = merge(
    {
        name: "Front end code",
        entry: {
            app: PATHS.app,
        },
        output: {
            path: PATHS.build,
            filename: '[name].js',
        },
    
    },
    parts.loadIcons({
        options: {
            name: '[name].[ext]',
        }
    }),
    parts.devServer({
        contentBase: path.join(__dirname, 'client'),
        host: process.env.HOST,
        port: 8082,
    }),
    parts.generateSourceMaps({
        type: 'cheap-module-eval-inline-source-map',
    }),
    parts.loadCSS(),
    parts.loadSCSS(),
);

const productionConfig = merge(
    {
        name: 'Front end code',
        entry: {
            path: PATHS.build,
            filename: '[name].js'
        },
    },
    parts.loadCSS(),
    parts.loadSCSS(),
    parts.loadIcons()
);

module.exports = (env) => {
    if (env == "production") {
        return merge(commonConfig, productionConfig);
    }

    return merge(commonConfig, developmentConfig);
}