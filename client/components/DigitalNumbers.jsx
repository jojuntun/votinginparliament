// https://github.com/acarl005/boggle-ui/blob/master/src/js/components/react-digital-numbers.jsx
// 15.12.2020 (modified a bit)
import React from 'react';

const classes = {
  '0': 'zero',
  '1': 'one',
  '2': 'two',
  '3': 'three',
  '4': 'four',
  '5': 'five',
  '6': 'six',
  '7': 'seven',
  '8': 'eight',
  '9': 'nine',
  ':': 'dots'
};

const Digit = (props) => 
    <div className={props.digit}>
        <span className="d1"></span>
        <span className="d2"></span>
        <span className="d3"></span>
        <span className="d4"></span>
        <span className="d5"></span>
        <span className="d6"></span>
        <span className="d7"></span>
    </div>;

const DigitalNumbers = (props) => {

    let nums = props.numbers;
    let display = [], i = 0;
    for (let digit of nums) {
      display.push(<Digit digit={classes[digit]} key={i++}/>);
    }
    return (
      <div className="react-digital-nums">
        <div className="digits">
          { display }
          <section className="units">
            { props.units }
          </section>
        </div>
      </div>
    );
  };

module.exports = DigitalNumbers;