import React from 'react';

import Voter from './Voter.jsx';
import DigitalNumbers from './DigitalNumbers.jsx';

import { Activity, Virus, TriangleSquareCircle, PlaneArrival } from 'tabler-icons-react';

import "./styles.css";
import "./styles.scss";

var colors = [
    "#660000", // Vasemmistoliitto
    "#FF3333", // SDP
    "#33FF33", // Vihreät
    "#FFF000", // RKP
    "#009900", // Keskusta
    "#66FFFF", // KD
    "#3399FF", // Kokoomus
    "#FF3399", // Liike Nyt
    "#FF8000"  // PS
]; 

var parties = [
    {
        party: "Vasemmistoliitto",
        members: 16,
        color: colors[0]
    },
    {
        party: "SDP",
        members: 40,
        color: colors[1]
    },
    {
        party: "Vihreät",
        members: 20,
        color: colors[2]
    },
    {
        party: "RKP",
        members: 10,
        color: colors[3]
    },
    {
        party: "Keskusta",
        members: 31,
        color: colors[4]
    },
    {
        party: "KD",
        members: 5,
        color: colors[5]
    },
    {
        party: "Kokoomus",
        members: 38,
        color: colors[6]
    },
    {
        party: "Liike Nyt",
        members: 1,
        color: colors[7]
    },
    {
        party: "PS",
        members: 38,
        color: colors[8]
    }
]

function getPartyListItems() {
    var elements = [];
    for (var i = 0; i < parties.length; i++) {
        let party = parties[i];
        let partyMembersCount = party.members;
        for (var j = 0; j < partyMembersCount; j++) {
            elements.push(<li key={i*parties.length + j}><Voter bgColor={party.color} /></li>)
        }
    }

    return elements;
}

export default class App extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="content-area">
                <div className="voters-area">
                    {/* <Voter bgColor={colors[2]} /> */}
                    <ul className="circle-container">
                        {/* <li><img src="https://pix10.agoda.net/hotelImages/3375/-1/f73547b49eadee36c6346f52a5b4f4fe.jpg?s=1024x768" alt="..." /></li> */}
                        {getPartyListItems()}
                    </ul>
                </div>
                <div className="result-area">
                    <table>
                        <tr>
                            <td>JAA</td>
                            <td>
                                <DigitalNumbers color={'#ff0000'} numbers={[9,8]} />
                            </td>
                        </tr>
                        <tr>
                            <td>EI</td>
                            <td>
                                <DigitalNumbers color={'#ff0000'} numbers={[8,7]} />
                            </td>
                        </tr>
                        <tr>
                            <td>TYHJIÄ</td>
                            <td>
                                <DigitalNumbers color={'#ff0000'} numbers={[7]} />
                            </td>
                        </tr>
                        <tr>
                            <td>POISSA</td>
                            <td>
                                <DigitalNumbers color={'#ff0000'} numbers={[8]} />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
        );
    }
}