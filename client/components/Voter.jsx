import React from 'react';

// https://codepen.io/piratheepan/pen/mmKYav?css-preprocessor=none&__cf_chl_jschl_tk__=5a8a07ca5b3fd4b13727cbce01629fe64c81933c-1605448002-0-AayharvqqwHBXLTJjooUXcW-z3n_mEF_K04HTm3YvQPrXCRciuZhVFmxBvD867Fn87eSBAM-W1xrOmVzfCA4Rzfwyzyt2ORZmRsW73oCLtilZldN2xWAek2pc8XSXF0rwZHQ-WzZxVqsQx77EWRCdjaPn1zijKsDfP6PaHA3M6lGdxWR-ST-ObyeuGP8z5qrqYFWd05Q52KcaDSHGe1qDeYflAkdbJGRrx7TWKsLCVWY1R26yDQasKLGu-74f7dfG5U7-1aeUrOVMcuQkD2hc1cqlZEl2om7sdGNUwd3xnExO6N1Hj7CVZZud1oxwNxiXDn2qdNCHHP5cJaIWcpifvZJBdc9FYqaZQg8KhnDmC6QLPjCH0C3bI_mFDra6fdnR6Ssq4gU8bVcqHpyFBxw-C8
// (15.11.2020)
export default function Voter(props) {
    var circleStyle = {
        position: "absolute",
        // padding: 10,
        // margin: 20,
        display: 'inline-block',
        backgroundColor: props.bgColor,
        borderRadius: "50%",
        borderColor: "red",
        borderStyle: "solid",
        boxSizing: "border-box",
        borderWidth: 2,
        boxShadow: "0 0 0 3px "  + props.bgColor,
        // width: 10,
        // height: 10,
        width: "100%",
        height: "100%",
    };

    return (
        <div style={circleStyle} />
    )
}